# web-api

[![CI Status](https://img.shields.io/travis/Edgar Froylan Rodriguez Mondragon/web-api.svg?style=flat)](https://travis-ci.org/Edgar Froylan Rodriguez Mondragon/web-api)
[![Version](https://img.shields.io/cocoapods/v/web-api.svg?style=flat)](https://cocoapods.org/pods/web-api)
[![License](https://img.shields.io/cocoapods/l/web-api.svg?style=flat)](https://cocoapods.org/pods/web-api)
[![Platform](https://img.shields.io/cocoapods/p/web-api.svg?style=flat)](https://cocoapods.org/pods/web-api)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

web-api is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'web-api'
```

## Author

Edgar Froylan Rodriguez Mondragon, edddea@gmail.com

## License

web-api is available under the MIT license. See the LICENSE file for more info.
